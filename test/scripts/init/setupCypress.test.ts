import { afterEach, beforeEach, test, expect } from 'vitest';
import path from 'path';
import fs from 'fs/promises';
import { match, P } from 'ts-pattern';
import { setupCypress } from '../../../src/scripts/init/setupCypress';
import { fileExists } from '../../../src/utils/fsUtils';

const WORKING_DIR = path.join(
    __dirname,
    '..',
    '..',
    '__working_directories__',
    'setupCypress'
);
const CYPRESS_FILE = 'cypress.config.ts';
const CYPRESS_SUPPORT_DIR = path.join(WORKING_DIR, 'cypress', 'support');
const CYPRESS_COMMANDS_FILE = path.join(CYPRESS_SUPPORT_DIR, 'commands.ts');

const VALID_CONFIG_CONTENT = `
import { defineConfig } from '@craigmiller160/js-config/cypress/cypress.config.js';

export default defineConfig({});
`.trim();

const INVALID_CONFIG_CONTENT = `
import { defineConfig } from 'cypress';

export default defineConfig({});
`.trim();

const VALID_COMMAND_CONTENT = `
import '@craigmiller160/js-config/cypress/addScreenshotToReport.js';
`.trim();
const INVALID_COMMAND_CONTENT = `
const abc = 'def';
`.trim();

type CypressConfigScenario =
    | 'no-cypress-config'
    | 'valid-cypress-config'
    | 'invalid-cypress-config'
    | 'project-without-cypress';

const clean = async () => {
    const files = await fs.readdir(WORKING_DIR);
    const promises = files
        .filter((file) => '.gitkeep' !== file)
        .map((file) => path.join(WORKING_DIR, file))
        .map((file) =>
            fs.rm(file, {
                recursive: true,
                force: true
            })
        );
    await Promise.all(promises);
};

beforeEach(async () => {
    await clean();
});

afterEach(async () => {
    await clean();
});

const setupCypressConfigFile = async (
    scenario: CypressConfigScenario
): Promise<void> => {
    const content: string | undefined = match<
        CypressConfigScenario,
        string | undefined
    >(scenario)
        .with('valid-cypress-config', () => VALID_CONFIG_CONTENT)
        .with('invalid-cypress-config', () => INVALID_CONFIG_CONTENT)
        .with(
            P.union('no-cypress-config', 'project-without-cypress'),
            () => undefined
        )
        .exhaustive();

    if (content) {
        await fs.writeFile(path.join(WORKING_DIR, CYPRESS_FILE), content);
    }
};

test.each<CypressConfigScenario>([
    'no-cypress-config',
    'valid-cypress-config',
    'project-without-cypress',
    'invalid-cypress-config'
])('Sets up cypress config for scenario %s', async (scenario) => {
    await setupCypressConfigFile(scenario);
    const result = setupCypress(
        WORKING_DIR,
        scenario !== 'project-without-cypress'
    );
    expect(result).toBeRight();

    const files = await fs.readdir(WORKING_DIR);
    const cypressConfigFile = files.find((file) => CYPRESS_FILE === file);
    if (scenario === 'project-without-cypress') {
        expect(cypressConfigFile).toBeUndefined();
        return;
    }

    expect(cypressConfigFile).not.toBeUndefined();
    if (cypressConfigFile === undefined) {
        // Ensures typescript treats variables as non-undefined
        throw new Error();
    }

    const content = await fs.readFile(
        path.join(WORKING_DIR, cypressConfigFile),
        'utf8'
    );
    expect(content).toEqual(VALID_CONFIG_CONTENT);
});

type CypressCommandsScenario =
    | 'project-without-cypress'
    | 'no-commands-file'
    | 'valid-commands-file'
    | 'invalid-commands-file';

const setupCypressCommandsFile = async (scenario: CypressCommandsScenario) => {
    const content = match<CypressCommandsScenario, string | undefined>(scenario)
        .with('valid-commands-file', () => VALID_COMMAND_CONTENT)
        .with('invalid-commands-file', () => INVALID_COMMAND_CONTENT)
        .with(
            P.union('no-commands-file', 'project-without-cypress'),
            () => undefined
        )
        .exhaustive();

    if (content) {
        await fs.mkdir(CYPRESS_SUPPORT_DIR, {
            recursive: true
        });
        await fs.writeFile(CYPRESS_COMMANDS_FILE, content);
    }
};

const cypressCommandsExistsScenario: ReadonlyArray<CypressCommandsScenario> = [
    'valid-commands-file',
    'invalid-commands-file'
];

test.each<CypressCommandsScenario>([
    'no-commands-file',
    'valid-commands-file',
    'invalid-commands-file',
    'project-without-cypress'
])('Sets up cypress commands for scenario %s', async (scenario) => {
    await setupCypressCommandsFile(scenario);
    const result = setupCypress(
        WORKING_DIR,
        scenario !== 'project-without-cypress'
    );
    expect(result).toBeRight();

    const commandFileExists = await fileExists(CYPRESS_COMMANDS_FILE)();
    if (!cypressCommandsExistsScenario.includes(scenario)) {
        expect(commandFileExists).toEqualRight(false);
        return;
    }

    expect(commandFileExists).toEqualRight(true);
    const content = await fs.readFile(CYPRESS_COMMANDS_FILE, 'utf8');
    expect(content.includes(VALID_COMMAND_CONTENT)).toBe(true);
});
