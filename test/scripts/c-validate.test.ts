import { beforeEach, expect, MockedFunction, vi, test } from 'vitest';
import { runCommandSync } from '../../src/scripts/utils/runCommand';
import { execute } from '../../src/scripts/c-validate';
import { either } from 'fp-ts';
import {
    parseControlFile,
    ControlFile
} from '../../src/scripts/files/ControlFile';
import { match, P } from 'ts-pattern';

const runCommandSyncMock = runCommandSync as MockedFunction<
    typeof runCommandSync
>;

const parseControlFileMock: MockedFunction<typeof parseControlFile> = vi.fn();

beforeEach(() => {
    vi.resetAllMocks();
});

test.each<Pick<ControlFile, 'directories'>>([
    { directories: { test: false, cypress: false } },
    { directories: { test: true, cypress: false } },
    { directories: { test: false, cypress: true } },
    { directories: { test: true, cypress: true } }
])('c-validate with $directories', ({ directories }) => {
    const controlFile: ControlFile = {
        directories,
        workingDirectoryPath: '',
        projectType: 'module',
        eslintPlugins: {
            cypress: false,
            vitest: false,
            jestDom: false,
            tanstackQuery: false,
            testingLibraryReact: false,
            react: false
        }
    };
    parseControlFileMock.mockReturnValue(either.right(controlFile));
    runCommandSyncMock.mockReturnValue(either.right(''));
    execute(process, parseControlFileMock);

    const numberOfCalls = match(directories)
        .with({ test: false, cypress: false }, () => 3)
        .with(
            P.union(
                { test: true, cypress: false },
                { test: false, cypress: true }
            ),
            () => 4
        )
        .with({ test: true, cypress: true }, () => 5)
        .exhaustive();

    expect(runCommandSyncMock).toHaveBeenCalledTimes(numberOfCalls);
    expect(runCommandSyncMock).toHaveBeenNthCalledWith(1, 'c-type-check');
    expect(runCommandSyncMock).toHaveBeenNthCalledWith(2, 'c-eslint');
    expect(runCommandSyncMock).toHaveBeenNthCalledWith(3, 'c-stylelint');

    let callCounter = 3;
    if (directories.test) {
        callCounter++;
        expect(runCommandSyncMock).toHaveBeenNthCalledWith(
            callCounter,
            'c-test'
        );
    }

    if (directories.cypress) {
        callCounter++;
        expect(runCommandSyncMock).toHaveBeenNthCalledWith(
            callCounter,
            'c-cypress'
        );
    }
});

type ExcludeCommand =
    | 'c-type-check'
    | 'c-eslint'
    | 'c-stylelint'
    | 'c-test'
    | 'c-cypress'
    | 'all';

const excludeCommandArgs: Record<Exclude<ExcludeCommand, 'all'>, string> = {
    'c-type-check': '-x=c-type-check',
    'c-eslint': '-x=c-eslint',
    'c-stylelint': '-x=c-stylelint',
    'c-test': '-x=c-test',
    'c-cypress': '-x=c-cypress'
};

const getExcludeCommandArgs = (
    scenario: ExcludeCommand
): ReadonlyArray<string> => {
    if (scenario === 'all') {
        return Object.values(excludeCommandArgs);
    }
    return [excludeCommandArgs[scenario]];
};

test.each<ExcludeCommand>([
    'c-cypress',
    'c-type-check',
    'c-eslint',
    'c-stylelint',
    'c-test',
    'all'
])('Runs c-validate excluding specific command(s): %s', (scenario) => {
    const controlFile: ControlFile = {
        directories: {
            test: true,
            cypress: true
        },
        workingDirectoryPath: '',
        projectType: 'module',
        eslintPlugins: {
            cypress: false,
            vitest: false,
            jestDom: false,
            tanstackQuery: false,
            testingLibraryReact: false,
            react: false
        }
    };
    parseControlFileMock.mockReturnValue(either.right(controlFile));
    runCommandSyncMock.mockReturnValue(either.right(''));
    const args = getExcludeCommandArgs(scenario);
    execute(
        {
            ...process,
            argv: ['', '', ...args]
        },
        parseControlFileMock
    );

    if (scenario === 'all') {
        expect(runCommandSyncMock).toHaveBeenCalledTimes(0);
        return;
    }

    expect(runCommandSyncMock).toHaveBeenCalledTimes(4);
    if (scenario !== 'c-cypress') {
        expect(runCommandSyncMock).toHaveBeenCalledWith('c-cypress');
    } else {
        expect(runCommandSyncMock).not.toHaveBeenCalledWith('c-cypress');
    }

    if (scenario !== 'c-type-check') {
        expect(runCommandSyncMock).toHaveBeenCalledWith('c-type-check');
    } else {
        expect(runCommandSyncMock).not.toHaveBeenCalledWith('c-type-check');
    }

    if (scenario !== 'c-eslint') {
        expect(runCommandSyncMock).toHaveBeenCalledWith('c-eslint');
    } else {
        expect(runCommandSyncMock).not.toHaveBeenCalledWith('c-eslint');
    }

    if (scenario !== 'c-stylelint') {
        expect(runCommandSyncMock).toHaveBeenCalledWith('c-stylelint');
    } else {
        expect(runCommandSyncMock).not.toHaveBeenCalledWith('c-stylelint');
    }

    if (scenario !== 'c-test') {
        expect(runCommandSyncMock).toHaveBeenCalledWith('c-test');
    } else {
        expect(runCommandSyncMock).not.toHaveBeenCalledWith('c-test');
    }
});
