import {
    afterEach,
    beforeEach,
    expect,
    MockedFunction,
    vi,
    test
} from 'vitest';
import path from 'path';
import { CYPRESS } from '../../src/scripts/commandPaths';
import { runCommandSync } from '../../src/scripts/utils/runCommand';
import { either, taskEither } from 'fp-ts';
import { execute } from '../../src/scripts/c-cypress-dev';
import { compileAndGetCypressConfig } from '../../src/scripts/cypress';
import fs from 'fs';

vi.mock('../../src/scripts/cypress', async () => {
    const actual = await vi.importActual('../../src/scripts/cypress');
    return {
        ...actual,
        compileAndGetCypressConfig: vi.fn()
    };
});

const runCommandSyncMock = runCommandSync as MockedFunction<
    typeof runCommandSync
>;

const compileAndGetCypressConfigMock =
    compileAndGetCypressConfig as MockedFunction<
        typeof compileAndGetCypressConfig
    >;

const WORKING_DIR = path.join(
    __dirname,
    '..',
    '__working_directories__',
    'cypressScript'
);
const ENVIRONMENT_DIR = path.join(WORKING_DIR, 'environment');

const COMMAND = path.join(WORKING_DIR, 'node_modules', CYPRESS);
const CONFIG = path.join(process.cwd(), 'cypress.config.js');

const clean = () => {
    if (fs.existsSync(ENVIRONMENT_DIR)) {
        fs.rmSync(ENVIRONMENT_DIR, {
            recursive: true,
            force: true
        });
    }
};

beforeEach(() => {
    vi.resetAllMocks();
    clean();
});

afterEach(() => {
    clean();
});

test('runs command', async () => {
    runCommandSyncMock.mockReturnValue(either.right(''));
    compileAndGetCypressConfigMock.mockReturnValue(taskEither.right(CONFIG));
    await execute({
        ...process,
        cwd: () => WORKING_DIR,
        env: {
            DEFAULT: 'foo'
        }
    });

    expect(runCommandSyncMock).toHaveBeenCalledWith(
        `${COMMAND} open -C ${CONFIG}`,
        {
            env: {
                DEFAULT: 'foo'
            }
        }
    );
});

test('runs command with env variables', async () => {
    runCommandSyncMock.mockReturnValue(either.right(''));
    compileAndGetCypressConfigMock.mockReturnValue(taskEither.right(CONFIG));

    fs.mkdirSync(ENVIRONMENT_DIR);
    const envFile = path.join(ENVIRONMENT_DIR, '.env.cypress');
    fs.writeFileSync(envFile, 'VITE_HELLO=world');

    await execute({
        ...process,
        cwd: () => WORKING_DIR,
        env: {
            DEFAULT: 'foo'
        }
    });

    expect(runCommandSyncMock).toHaveBeenCalledWith(
        `${COMMAND} open -C ${CONFIG}`,
        {
            env: {
                DEFAULT: 'foo',
                VITE_HELLO: 'world'
            }
        }
    );
});
