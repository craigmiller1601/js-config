import {
    afterEach,
    beforeEach,
    expect,
    MockedFunction,
    test,
    vi
} from 'vitest';
import { runCommandSync } from '../../src/scripts/utils/runCommand';
import path from 'path';
import fs from 'fs';
import {
    CYPRESS,
    MARGE,
    MOCHAWESOME_MERGE
} from '../../src/scripts/commandPaths';
import { either, taskEither } from 'fp-ts';
import { execute } from '../../src/scripts/c-cypress';
import { compileAndGetCypressConfig } from '../../src/scripts/cypress';
import { match } from 'ts-pattern';

vi.mock('../../src/scripts/cypress', async () => {
    const actual = await vi.importActual('../../src/scripts/cypress');
    return {
        ...actual,
        compileAndGetCypressConfig: vi.fn()
    };
});

const runCommandSyncMock = runCommandSync as MockedFunction<
    typeof runCommandSync
>;
const compileAndGetCypressConfigMock =
    compileAndGetCypressConfig as MockedFunction<
        typeof compileAndGetCypressConfig
    >;

const WORKING_DIR = path.join(
    __dirname,
    '..',
    '__working_directories__',
    'cypressScript'
);
const ENVIRONMENT_DIR = path.join(WORKING_DIR, 'environment');

const CYPRESS_COMMAND = path.join(WORKING_DIR, 'node_modules', CYPRESS);
const MOCHAWESOME_MERGE_COMMAND = path.join(
    WORKING_DIR,
    'node_modules',
    MOCHAWESOME_MERGE
);
const MARGE_COMMAND = path.join(WORKING_DIR, 'node_modules', MARGE);
const CONFIG = path.join(process.cwd(), 'cypress.config.js');
const RESULTS_JSON_PATH = path.join(
    WORKING_DIR,
    'cypress',
    'results',
    'json',
    '*.json'
);
const REPORT_PATH = path.join(WORKING_DIR, 'cypress', 'results', 'report');
const REPORT_JSON_PATH = path.join(
    WORKING_DIR,
    'cypress',
    'results',
    'report.json'
);

const clean = () => {
    if (fs.existsSync(ENVIRONMENT_DIR)) {
        fs.rmSync(ENVIRONMENT_DIR, {
            recursive: true,
            force: true
        });
    }
};

beforeEach(() => {
    vi.resetAllMocks();
    clean();
});

afterEach(() => {
    clean();
});

type Scenario =
    | 'run-standard'
    | 'run-parallel-index-0'
    | 'run-parallel-index-1'
    | 'run-parallel-index-2';

const getArgs = (scenario: Scenario): ReadonlyArray<string> =>
    match(scenario)
        .with('run-parallel-index-0', () => [
            '--parallel-total=3',
            '--parallel-index=0'
        ])
        .with('run-parallel-index-1', () => [
            '--parallel-total=3',
            '--parallel-index=1'
        ])
        .with('run-parallel-index-2', () => [
            '--parallel-total=3',
            '--parallel-index=2'
        ])
        .with('run-standard', () => [])
        .exhaustive();

const getSpecs = (scenario: Scenario): string => {
    const specs = match<Scenario, ReadonlyArray<string>>(scenario)
        .with('run-standard', () => [
            'cypress/component/*.cy.{js,ts,jsx,tsx,mjs,cjs,mts,cts}'
        ])
        .with('run-parallel-index-0', () => [
            'cypress/component/01.cy.ts',
            'cypress/component/04.cy.ts',
            'cypress/component/07.cy.ts',
            'cypress/component/10.cy.ts'
        ])
        .with('run-parallel-index-1', () => [
            'cypress/component/02.cy.ts',
            'cypress/component/05.cy.ts',
            'cypress/component/08.cy.ts'
        ])
        .with('run-parallel-index-2', () => [
            'cypress/component/03.cy.ts',
            'cypress/component/06.cy.ts',
            'cypress/component/09.cy.ts'
        ])
        .exhaustive();

    return specs.toSorted((a, b) => a.localeCompare(b)).join(',');
};

test.each<Scenario>([
    'run-standard',
    'run-parallel-index-0',
    'run-parallel-index-1',
    'run-parallel-index-2'
])('runs cypress command for scenario %s', async (scenario) => {
    runCommandSyncMock.mockReturnValue(either.right(''));
    compileAndGetCypressConfigMock.mockReturnValue(taskEither.right(CONFIG));
    const args = getArgs(scenario);
    await execute({
        ...process,
        argv: ['', '', ...args],
        cwd: () => WORKING_DIR,
        env: {
            DEFAULT: 'foo'
        }
    });

    const specs = getSpecs(scenario);

    expect(compileAndGetCypressConfigMock).toHaveBeenCalled();
    expect(runCommandSyncMock).toHaveBeenCalledTimes(3);
    expect(runCommandSyncMock).toHaveBeenNthCalledWith(
        1,
        `${CYPRESS_COMMAND} run --component -b chrome -C ${CONFIG} --spec '${specs}'`,
        {
            env: {
                DEFAULT: 'foo'
            }
        }
    );
    expect(runCommandSyncMock).toHaveBeenNthCalledWith(
        2,
        `${MOCHAWESOME_MERGE_COMMAND} ${RESULTS_JSON_PATH} > ${REPORT_JSON_PATH}`,
        {
            shell: true
        }
    );
    expect(runCommandSyncMock).toHaveBeenNthCalledWith(
        3,
        `${MARGE_COMMAND} ${REPORT_JSON_PATH} -o ${REPORT_PATH}`
    );
});

test('runs with environment variables', async () => {
    runCommandSyncMock.mockReturnValue(either.right(''));
    compileAndGetCypressConfigMock.mockReturnValue(taskEither.right(CONFIG));
    const specs = getSpecs('run-standard');

    fs.mkdirSync(ENVIRONMENT_DIR);
    const envFile = path.join(ENVIRONMENT_DIR, '.env.cypress');
    fs.writeFileSync(envFile, 'VITE_HELLO=world');

    await execute({
        ...process,
        cwd: () => WORKING_DIR,
        env: {
            DEFAULT: 'foo'
        }
    });

    expect(compileAndGetCypressConfigMock).toHaveBeenCalled();
    expect(runCommandSyncMock).toHaveBeenCalledTimes(3);
    expect(runCommandSyncMock).toHaveBeenNthCalledWith(
        1,
        `${CYPRESS_COMMAND} run --component -b chrome -C ${CONFIG} --spec '${specs}'`,
        {
            env: {
                DEFAULT: 'foo',
                VITE_HELLO: 'world'
            }
        }
    );
    expect(runCommandSyncMock).toHaveBeenNthCalledWith(
        2,
        `${MOCHAWESOME_MERGE_COMMAND} ${RESULTS_JSON_PATH} > ${REPORT_JSON_PATH}`,
        {
            shell: true
        }
    );
    expect(runCommandSyncMock).toHaveBeenNthCalledWith(
        3,
        `${MARGE_COMMAND} ${REPORT_JSON_PATH} -o ${REPORT_PATH}`
    );
});
