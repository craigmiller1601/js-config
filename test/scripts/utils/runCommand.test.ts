import { describe, it, expect, vi } from 'vitest';
import {
    runCommandAsync,
    runCommandSync
} from '../../../src/scripts/utils/runCommand';
import { either } from 'fp-ts';

vi.unmock('../../../src/scripts/utils/runCommand');

const MAC_ERROR_MESSAGE = 'ls: package.json2: No such file or directory';
const DEBIAN_ERROR_MESSAGE =
    "ls: cannot access 'package.json2': No such file or directory";

describe('runCommand', () => {
    describe('runCommandAsync', () => {
        it('runs command successfully with default options', async () => {
            const result = await runCommandAsync('ls -l package.json', {
                stdio: 'pipe'
            })();
            expect(result).toBeRight();
            const value = (result as either.Right<string>).right;
            expect(value.trim().endsWith('package.json')).toBe(true);
        });

        it('runs command with error with default options', async () => {
            const result = await runCommandAsync('ls -l package.json2', {
                stdio: 'pipe'
            })();
            const errorMessage = process.env.CI_PIPELINE_ID
                ? DEBIAN_ERROR_MESSAGE
                : MAC_ERROR_MESSAGE;
            const errorCode = process.env.CI_PIPELINE_ID ? '2' : '1';
            expect(result).toEqualLeft(
                new Error(
                    `Command failed. Status: ${errorCode} Message: ${errorMessage}`
                )
            );
        });
    });

    describe('runCommandSync', () => {
        it('runs command successfully with default options', () => {
            const result = runCommandSync('ls -l package.json', {
                stdio: 'pipe'
            });
            expect(result).toBeRight();
            const value = (result as either.Right<string>).right;
            expect(value.trim().endsWith('package.json')).toBe(true);
        });

        it('runs command with error with default options', () => {
            const result = runCommandSync('ls -l package.json2', {
                stdio: 'pipe'
            });
            const errorMessage = process.env.CI_PIPELINE_ID
                ? DEBIAN_ERROR_MESSAGE
                : MAC_ERROR_MESSAGE;
            const errorCode = process.env.CI_PIPELINE_ID ? '2' : '1';
            expect(result).toEqualLeft(
                new Error(
                    `Command failed. Status: ${errorCode} Message: ${errorMessage}\n`
                )
            );
        });
    });
});
