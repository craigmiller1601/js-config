import fs from 'fs/promises';
import { taskEither, function as func } from 'fp-ts';

type FileAccessError = Error &
    Readonly<{
        code: string;
    }>;

const handleFileAccessError = (
    ex: FileAccessError
): taskEither.TaskEither<Error, boolean> => {
    if (ex.code === 'ENOENT') {
        return taskEither.right(false);
    }
    return taskEither.left(ex);
};

export const fileExists = (
    file: string
): taskEither.TaskEither<Error, boolean> => {
    fs.access(file)
        .then(() => true)
        .catch((ex: FileAccessError) => {
            if (ex.code === 'ENOENT') {
                return false;
            }
            throw ex;
        });
    return func.pipe(
        taskEither.tryCatch(
            () => fs.access(file),
            (ex) => ex as FileAccessError
        ),
        taskEither.fold(handleFileAccessError, () => taskEither.right(true))
    );
};
