import { logger } from './logger';
import { either, function as func, readonlyArray } from 'fp-ts';
import { runCommandSync } from './utils/runCommand';
import { terminate } from './utils/terminate';
import {
    ControlFile,
    parseControlFile as defaultParseControlFile
} from './files/ControlFile';

type ShouldRunCommand = Readonly<{
    'c-type-check': boolean;
    'c-eslint': boolean;
    'c-stylelint': boolean;
    'c-test': boolean;
    'c-cypress': boolean;
}>;
const EXCLUSION_ARG_REGEX = /^-x=(?<command>.+)$/;
type ExclusionGroups = Readonly<{
    command: keyof ShouldRunCommand;
}>;

const getExclusionArgs = (
    argv: ReadonlyArray<string>
): ReadonlyArray<keyof ShouldRunCommand> =>
    argv
        .slice(2)
        .map(
            (arg) =>
                EXCLUSION_ARG_REGEX.exec(arg)?.groups as
                    | ExclusionGroups
                    | undefined
        )
        .filter((groups): groups is ExclusionGroups => !!groups)
        .map((groups) => groups.command);

const getShouldRunCommand = (
    argv: ReadonlyArray<string>,
    controlFile: ControlFile
): ShouldRunCommand => {
    const exclusions = getExclusionArgs(argv);
    return {
        'c-eslint': !exclusions.includes('c-eslint'),
        'c-type-check': !exclusions.includes('c-type-check'),
        'c-stylelint': !exclusions.includes('c-stylelint'),
        'c-cypress':
            !exclusions.includes('c-cypress') &&
            controlFile.directories.cypress,
        'c-test': !exclusions.includes('c-test') && controlFile.directories.test
    };
};

const runCommands = (
    shouldRunCommand: ShouldRunCommand
): either.Either<Error, string> => {
    const commandExecutions: ReadonlyArray<
        (() => either.Either<Error, string>) | undefined
    > = [
        shouldRunCommand['c-type-check']
            ? () => runCommandSync('c-type-check')
            : undefined,
        shouldRunCommand['c-eslint']
            ? () => runCommandSync('c-eslint')
            : undefined,
        shouldRunCommand['c-stylelint']
            ? () => runCommandSync('c-stylelint')
            : undefined,
        shouldRunCommand['c-test'] ? () => runCommandSync('c-test') : undefined,
        shouldRunCommand['c-cypress']
            ? () => runCommandSync('c-cypress')
            : undefined
    ];

    return func.pipe(
        commandExecutions,
        readonlyArray.filter(
            (execution): execution is () => either.Either<Error, string> =>
                !!execution
        ),
        readonlyArray.reduce(
            () => either.right<Error, string>(''),
            (acc, execution) => () =>
                func.pipe(
                    acc(),
                    either.chain(() => execution())
                )
        )
    )();
};

export const execute = (
    process: NodeJS.Process,
    parseControlFile: typeof defaultParseControlFile = defaultParseControlFile
) => {
    logger.info('Running full validation');

    func.pipe(
        parseControlFile(process),
        either.map((controlFile) =>
            getShouldRunCommand(process.argv, controlFile)
        ),
        either.chain(runCommands),
        either.fold(terminate, terminate)
    );
};
