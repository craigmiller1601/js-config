import { runCommandSync } from './utils/runCommand';
import { either, function as func } from 'fp-ts';
import { terminate } from './utils/terminate';
import { findCommand } from './utils/command';
import { VITEST } from './commandPaths';
import { logger } from './logger';
import { getRealArgs } from './utils/process';

export const execute = (process: NodeJS.Process) => {
    logger.info('Running unit tests');
    const args = getRealArgs(process).join(' ');
    func.pipe(
        findCommand(process, VITEST),
        either.chain((command) =>
            runCommandSync(`${command} run ${args}`.trim())
        ),
        either.fold(terminate, terminate)
    );
};
