import { logger } from './logger';
import { function as func, taskEither } from 'fp-ts';
import { findCommand } from './utils/command';
import { CYPRESS } from './commandPaths';
import { runCommandSync } from './utils/runCommand';
import { terminate } from './utils/terminate';
import { compileAndGetCypressConfig, getEnv } from './cypress';

export const execute = (process: NodeJS.Process): Promise<void> => {
    logger.info('Running cypress dev server');
    return func.pipe(
        findCommand(process, CYPRESS),
        taskEither.fromEither,
        taskEither.bindTo('command'),
        taskEither.bind('config', () => compileAndGetCypressConfig(process)),
        taskEither.bind('env', () => getEnv(process)),
        taskEither.chainEitherK(({ command, config, env }) =>
            runCommandSync(`${command} open -C ${config}`, {
                env
            })
        ),
        taskEither.fold(
            (ex) => () => {
                terminate(ex);
                return Promise.resolve();
            },
            () => () => {
                terminate('');
                return Promise.resolve();
            }
        )
    )();
};
