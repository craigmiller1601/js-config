import { either, function as func } from 'fp-ts';
import { logger } from '../logger';
import fs from 'fs';
import path from 'path';

const CONFIG_SOURCE = '@craigmiller160/js-config/cypress/cypress.config.js';
const ADD_SCREENSHOT_COMMAND = `import '@craigmiller160/js-config/cypress/addScreenshotToReport.js';`;

const CONTENT = `
import { defineConfig } from '${CONFIG_SOURCE}';

export default defineConfig({});
`.trim();

const getCypressConfigPath = (cwd: string): string =>
    path.join(cwd, 'cypress.config.ts');
const getCypressCommandsPath = (cwd: string): string =>
    path.join(cwd, 'cypress', 'support', 'commands.ts');

type FileStatus = 'missing' | 'valid' | 'invalid';

const getCypressConfigStatus = (
    cwd: string
): either.Either<Error, FileStatus> =>
    either.tryCatch(() => {
        const cypressConfigPath = getCypressConfigPath(cwd);
        if (!fs.existsSync(cypressConfigPath)) {
            return 'missing';
        }

        const content = fs.readFileSync(cypressConfigPath, 'utf8');
        if (content.includes(CONFIG_SOURCE)) {
            return 'valid';
        }
        return 'invalid';
    }, either.toError);

const writeCypressConfig = (
    cwd: string,
    status: FileStatus
): either.Either<Error, void> => {
    if (status === 'valid') {
        logger.debug('Valid cypress config already exists');
        return either.right(func.constVoid());
    }

    logger.debug('Writing new cypress config');
    const cypressConfigPath = getCypressConfigPath(cwd);
    return either.tryCatch(
        () => fs.writeFileSync(cypressConfigPath, CONTENT),
        either.toError
    );
};

const getCypressCommandsStatus = (
    cwd: string
): either.Either<Error, FileStatus> =>
    either.tryCatch(() => {
        const cypressCommandsPath = getCypressCommandsPath(cwd);
        if (!fs.existsSync(cypressCommandsPath)) {
            return 'missing';
        }

        const content = fs.readFileSync(cypressCommandsPath, 'utf8');
        if (content.includes(ADD_SCREENSHOT_COMMAND)) {
            return 'valid';
        }
        return 'invalid';
    }, either.toError);

const writeCypressCommands = (
    cwd: string,
    status: FileStatus
): either.Either<Error, void> => {
    if (status === 'missing') {
        logger.debug('No cypress commands file present, not writing commands');
        return either.right(func.constVoid());
    }

    if (status === 'valid') {
        logger.debug('Valid cypress commands file already present');
        return either.right(func.constVoid());
    }

    logger.debug('Adding commands to cypress commands file');
    const cypressCommandsFilePath = getCypressCommandsPath(cwd);
    return either.tryCatch(() => {
        const content = fs.readFileSync(cypressCommandsFilePath, 'utf8');
        fs.writeFileSync(
            cypressCommandsFilePath,
            `
 ${ADD_SCREENSHOT_COMMAND}
 ${content}
    `.trim()
        );
    }, either.toError);
};

export const setupCypress = (
    cwd: string,
    hasCypress: boolean
): either.Either<Error, void> => {
    if (!hasCypress) {
        logger.info('Project does not have cypress, skipping setup');
        return either.right(func.constVoid());
    }
    logger.info('Setting up cypress');

    return func.pipe(
        getCypressConfigStatus(cwd),
        either.chain((status) => writeCypressConfig(cwd, status)),
        either.chain(() => getCypressCommandsStatus(cwd)),
        either.chain((status) => writeCypressCommands(cwd, status))
    );
};
