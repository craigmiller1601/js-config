import {
    either,
    function as func,
    option,
    readonlyArray,
    task,
    taskEither,
    taskOption
} from 'fp-ts';
import fs from 'fs/promises';
import path from 'path';
import { createCompile } from '../compile';
import dotenv from 'dotenv';

const CYPRESS_CONFIG_FILES: ReadonlyArray<string> = [
    'cypress.config.ts',
    'cypress.config.mts',
    'cypress.config.cts',
    'cypress.config.js',
    'cypress.config.mjs',
    'cypress.config.cjs'
];

const fileExists =
    (process: NodeJS.Process) =>
    (configFile: string): task.Task<string | undefined> => {
        const configFilePath = path.join(process.cwd(), configFile);
        return func.pipe(
            taskEither.tryCatch(() => fs.stat(configFilePath), func.identity),
            taskEither.fold(
                () => () => Promise.resolve(undefined),
                () => () => Promise.resolve(configFilePath)
            )
        );
    };

const compileCypressConfigFile =
    (process: NodeJS.Process) =>
    (configFile: string): taskEither.TaskEither<Error, string> => {
        const destDir = path.join(process.cwd(), 'node_modules');
        return createCompile(process.cwd(), destDir, 'commonjs')(configFile);
    };

export const compileAndGetCypressConfig = (
    process: NodeJS.Process
): taskEither.TaskEither<Error, string> =>
    func.pipe(
        CYPRESS_CONFIG_FILES,
        readonlyArray.map(fileExists(process)),
        task.sequenceArray,
        taskOption.fromTask,
        taskOption.chainOptionK(readonlyArray.findFirst((result) => !!result)),
        taskOption.chainOptionK(option.fromNullable),
        taskEither.fromTaskOption(
            () => new Error('Could not find cypress config file')
        ),
        taskEither.chain(compileCypressConfigFile(process))
    );

export const getEnv = (
    process: NodeJS.Process
): taskEither.TaskEither<Error, Record<string, string>> => {
    const envPath = path.join('environment', '.env.cypress');
    return func.pipe(
        fileExists(process)(envPath),
        taskEither.fromTask,
        taskEither.chainEitherK((fullFilePath) => {
            if (fullFilePath) {
                return either.tryCatch(
                    () =>
                        dotenv.config({
                            path: fullFilePath
                        }).parsed ?? {},
                    either.toError
                );
            }
            return either.right({});
        }),
        taskEither.map((env) => ({
            ...process.env,
            ...env
        }))
    );
};
