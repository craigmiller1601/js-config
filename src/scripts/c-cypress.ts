import { logger } from './logger';
import { either, function as func, taskEither } from 'fp-ts';
import { findCommand } from './utils/command';
import { CYPRESS, MARGE, MOCHAWESOME_MERGE } from './commandPaths';
import { runCommandSync } from './utils/runCommand';
import { terminate } from './utils/terminate';
import { compileAndGetCypressConfig, getEnv } from './cypress';
import path from 'path';
import fs from 'fs/promises';
import { fileExists } from '../utils/fsUtils';

type Args = Readonly<{
    parallelTotal: number;
    parallelIndex: number;
}>;

const getCypressResultsPath = (cwd: string): string =>
    path.join(cwd, 'cypress', 'results');
const getCypressJsonResultsPath = (cwd: string): string =>
    path.join(getCypressResultsPath(cwd), 'json', '*.json');
const getCypressReportPath = (cwd: string): string =>
    path.join(getCypressResultsPath(cwd), 'report');
const getCypressReportJsonPath = (cwd: string): string =>
    path.join(getCypressResultsPath(cwd), 'report.json');

const clearResults = (cwd: string): taskEither.TaskEither<Error, void> => {
    const resultsPath = getCypressResultsPath(cwd);
    return func.pipe(
        fileExists(resultsPath),
        taskEither.chain((exists) => {
            if (exists) {
                logger.debug('Deleting existing cypress results');
                return taskEither.tryCatch(
                    () =>
                        fs.rm(resultsPath, {
                            recursive: true,
                            force: true
                        }),
                    either.toError
                );
            }
            logger.debug('No existing cypress results to delete');
            return taskEither.right(func.constVoid());
        })
    );
};

const CYPRESS_FILE_REGEX = /^.+\.cy\.(js|ts|jsx|tsx|mjs|cjs|mts|cts)$/;

const getSpec = (
    cwd: string,
    args: Args
): taskEither.TaskEither<Error, string> => {
    const relativeCypressComponentRoot = path.join('cypress', 'component');
    const cypressComponentRoot = path.join(cwd, relativeCypressComponentRoot);
    if (args.parallelTotal === 0) {
        return taskEither.right(
            path.join(
                relativeCypressComponentRoot,
                '*.cy.{js,ts,jsx,tsx,mjs,cjs,mts,cts}'
            )
        );
    }

    return taskEither.tryCatch(async () => {
        const files = await fs.readdir(cypressComponentRoot);
        return files
            .filter((file) => CYPRESS_FILE_REGEX.test(file))
            .filter(
                (file, index) =>
                    index % args.parallelTotal === args.parallelIndex
            )
            .map((file) => path.join(relativeCypressComponentRoot, file))
            .join(',');
    }, either.toError);
};

const runCypress = (
    cwd: string,
    cypressCommand: string,
    mochawesomeMergeCommand: string,
    margeCommand: string,
    cypressConfig: string,
    args: Args,
    env: Record<string, string>
): taskEither.TaskEither<Error, void> => {
    const resultsJsonPath = getCypressJsonResultsPath(cwd);
    const reportPath = getCypressReportPath(cwd);
    const reportJsonPath = getCypressReportJsonPath(cwd);
    return func.pipe(
        getSpec(cwd, args),
        taskEither.chainFirst(() => clearResults(cwd)),
        taskEither.map((spec) =>
            func.pipe(
                runCommandSync(
                    `${cypressCommand} run --component -b chrome -C ${cypressConfig} --spec '${spec}'`,
                    {
                        env
                    }
                ),
                either.isRight
            )
        ),
        taskEither.chainFirstEitherK(() =>
            runCommandSync(
                `${mochawesomeMergeCommand} ${resultsJsonPath} > ${reportJsonPath}`,
                {
                    shell: true
                }
            )
        ),
        taskEither.chainFirstEitherK(() =>
            runCommandSync(`${margeCommand} ${reportJsonPath} -o ${reportPath}`)
        ),
        taskEither.chain((cypressSuccess) => {
            if (cypressSuccess) {
                return taskEither.right(func.constVoid());
            }
            return taskEither.left(
                new Error('Cypress suite failed, please check logs and report')
            );
        })
    );
};

const getArgs = (process: NodeJS.Process): Args => {
    const parallelTotal = process.argv
        .find((arg) => arg.startsWith('--parallel-total'))
        ?.split('=')?.[1];
    const parallelIndex = process.argv
        .find((arg) => arg.startsWith('--parallel-index'))
        ?.split('=')?.[1];
    return {
        parallelTotal: parallelTotal ? parseInt(parallelTotal) : 0,
        parallelIndex: parallelIndex ? parseInt(parallelIndex) : 0
    };
};

export const execute = (process: NodeJS.Process): Promise<void> => {
    logger.info('Running all cypress tests');
    const args = getArgs(process);
    return func.pipe(
        findCommand(process, CYPRESS),
        either.bindTo('cypressCommand'),
        either.bind('mochawesomeMergeCommand', () =>
            findCommand(process, MOCHAWESOME_MERGE)
        ),
        either.bind('margeCommand', () => findCommand(process, MARGE)),
        taskEither.fromEither,
        taskEither.bind('config', () => compileAndGetCypressConfig(process)),
        taskEither.bind('env', () => getEnv(process)),
        taskEither.chain(
            ({
                cypressCommand,
                margeCommand,
                mochawesomeMergeCommand,
                config,
                env
            }) =>
                runCypress(
                    process.cwd(),
                    cypressCommand,
                    mochawesomeMergeCommand,
                    margeCommand,
                    config,
                    args,
                    env
                )
        ),
        taskEither.fold(
            (ex) => () => {
                terminate(ex);
                return Promise.resolve();
            },
            () => () => {
                terminate('');
                return Promise.resolve();
            }
        )
    )();
};
