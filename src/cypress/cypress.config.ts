import { defineConfig as defaultDefineConfig } from 'cypress';

type DefineConfig = typeof defaultDefineConfig;

const DEFAULT_CONFIG = defaultDefineConfig({
    reporter: 'mochawesome',
    reporterOptions: {
        reportDir: 'cypress/results/json',
        overwrite: false,
        html: false,
        json: true
    },
    screenshotOnRunFailure: true,
    screenshotsFolder: 'cypress/results/report/assets',
    video: false,
    component: {
        devServer: {
            framework: 'react',
            bundler: 'vite'
        }
    },
    experimentalFetchPolyfill: true
});

export const defineConfig: DefineConfig = (config) => ({
    ...DEFAULT_CONFIG,
    ...config
});
