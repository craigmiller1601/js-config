import Mocha from 'mocha';
import addContext from 'mochawesome/addContext';

type SimpleStack = Readonly<{
    message: string;
    whitespace: string;
}>;
type DetailedStack = Readonly<{
    absoluteFile: string;
    column: number;
    fileUrl: string;
    function: string;
    line: number;
    originalFile: string;
    relativeFile: string;
    whitespace: string;
}>;
type Stack = SimpleStack | DetailedStack;

type CypressAttributes = Readonly<{
    body: string;
    currentRetry: number;
    duration: number;
    err: Readonly<{
        codeFrame: Readonly<{
            absoluteFile: string;
            column: number;
            frame: string;
            language: string;
            line: number;
            originalFile: string;
            relativeFile: string;
        }>;
        message: string;
        name: string;
        parsedStack: ReadonlyArray<Stack>;
        stack: string;
        type: string;
    }>;
    file: string | null;
    final: boolean;
    id: string;
    order: number;
    pending: boolean;
    state: string;
    retries: number;
    title: string;
    type: string;
    wallClockDuration: number;
    wallClockStartedAt: string;
}>;

const getScreenshotTitle = (runnable: Mocha.Test | Mocha.Suite): string => {
    if (runnable.parent) {
        const parentTitle = getScreenshotTitle(runnable.parent);
        if (parentTitle) {
            return `${parentTitle} -- ${runnable.title}`;
        }
    }

    return runnable.title;
};

const getAttempt = (attributes: CypressAttributes): string => {
    if (attributes.retries > 0) {
        return ` (attempt ${attributes.retries + 1})`;
    }
    return '';
};

const addScreenshotToReport = (
    attributes: CypressAttributes,
    runnable: Mocha.Test
) => {
    if (
        attributes.state === 'failed' &&
        attributes.currentRetry === attributes.retries
    ) {
        const title = getScreenshotTitle(runnable);
        const attempt = getAttempt(attributes);
        const screenshot = `assets/${Cypress.spec.name}/${title} (failed)${attempt}.png`;

        // @ts-expect-error This JS code works. However, the type definitions and documentation on how to use them are lacking. Just instantiating new Context() blows up this function. So this is being left here for now.
        addContext({ test: attributes }, screenshot);
    }
};

Cypress.on('test:after:run', addScreenshotToReport);
