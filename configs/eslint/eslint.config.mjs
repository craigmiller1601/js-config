// @ts-check

import eslintJs from '@eslint/js';
import eslintTs from 'typescript-eslint';
import globals from 'globals';
import eslintPrettier from 'eslint-plugin-prettier/recommended';
import eslintReact from 'eslint-plugin-react';
import eslintReactHooks from 'eslint-plugin-react-hooks';
import eslintJsxA11y from 'eslint-plugin-jsx-a11y';
import eslintVitest from '@vitest/eslint-plugin';
import eslintCypress from 'eslint-plugin-cypress/flat';
import eslintJestDom from 'eslint-plugin-jest-dom';
import eslintTestingLibrary from 'eslint-plugin-testing-library';
import eslintReactRefresh from 'eslint-plugin-react-refresh';
import eslintTanstackQuery from '@tanstack/eslint-plugin-query';
import eslintImport from 'eslint-plugin-import';
import oxlint from 'eslint-plugin-oxlint';
import path from 'path';
import fs from 'fs';

const fastEslint = process.env.ESLINT_FAST === 'true';

const controlFilePath = path.join(process.cwd(), '.js-config.json');
const controlFile = JSON.parse(fs.readFileSync(controlFilePath, 'utf8'));

const typescriptFileFilter = {
    files: ['**/*.{ts,tsx,mts,cts}']
};

const eslintTsConfigsRecommended = eslintTs.configs.recommended.map(
    (config) => ({
        ...typescriptFileFilter,
        ...config
    })
);
const eslintTsConfigsRecommendedTypeChecked =
    eslintTs.configs.recommendedTypeChecked.map((config) => ({
        ...typescriptFileFilter,
        ...config
    }));

const configs = [
    eslintJs.configs.recommended,
    eslintTsConfigsRecommended,
    eslintPrettier,
    oxlint.configs['flat/recommended'],
    {
        ignores: ['cypress/results/**/*'],
        languageOptions: {
            parserOptions: {
                ecmaVersion: 'esnext'
            }
        },
        languageOptions: {
            ecmaVersion: 'latest',
            globals: {
                ...globals.node,
                ...globals.browser
            }
        }
    }
];

if (!fastEslint) {
    configs.push(
        eslintTsConfigsRecommendedTypeChecked,
        eslintImport.flatConfigs.recommended,
        {
            ...typescriptFileFilter,
            languageOptions: {
                parserOptions: {
                    projectService: true,
                    tsconfigRootDir: process.cwd()
                }
            },
            rules: {
                '@typescript-eslint/no-misused-promises': [
                    'error',
                    {
                        checksVoidReturn: {
                            arguments: false,
                            attributes: false,
                            properties: true,
                            returns: true,
                            variables: true
                        }
                    }
                ],
                'import/named': 0,
                'import/namespace': 0,
                'import/default': 0,
                'import/no-named-as-default-member': 0
            }
        },
        {
            settings: {
                'import/resolver': {
                    typescript: true
                }
            }
        }
    );
}

if (controlFile.eslintPlugins.vitest) {
    configs.push({
        ...eslintVitest.configs.recommended,
        files: ['test/**/*.{js,jsx,ts,tsx,mjs,cjs,mts,cts}']
    });
}

if (controlFile.eslintPlugins.react) {
    configs.push(
        eslintReact.configs.flat.recommended,
        eslintReact.configs.flat['jsx-runtime'],
        eslintJsxA11y.flatConfigs.recommended,
        eslintReactRefresh.configs.recommended,
        {
            settings: {
                react: {
                    version: 'detect'
                }
            },
            plugins: {
                'react-hooks': eslintReactHooks
            },
            rules: {
                ...eslintReactHooks.configs.recommended.rules,
                'react-refresh/only-export-components': [
                    'error',
                    { allowConstantExport: true }
                ]
            }
        }
    );
}

if (controlFile.eslintPlugins.cypress) {
    configs.push({
        ...eslintCypress.configs.recommended,
        files: ['cypress/**/*.{js,jsx,ts,tsx,mjs,cjs,mts,cts}']
    });
}

if (controlFile.eslintPlugins.jestDom) {
    configs.push({
        files: ['test/**/*.{js,jsx,ts,tsx,mjs,cjs,mts,cts}'],
        ...eslintJestDom.configs['flat/recommended']
    });
}

if (controlFile.eslintPlugins.testingLibraryReact) {
    configs.push({
        files: ['test/**/*.{js,jsx,ts,tsx,mjs,cjs,mts,cts}'],
        ...eslintTestingLibrary.configs['flat/react']
    });
}

if (controlFile.eslintPlugins.tanstackQuery) {
    configs.push(...eslintTanstackQuery.configs['flat/recommended']);
}

export default eslintTs.config(...configs);
