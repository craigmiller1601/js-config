# JavaScript Configuration

A library that automatically sets up preferred configuration for any JavaScript project.

## Usage

1. Add this project with `pnpm add -D @craigmiller160/js-config`.
2. Add the following `postinstall` script to the `package.json`:

```json
{
  "scripts": {
    "postinstall": "c-init {arg} || echo 'No c-init'"
  }
}
```

### c-init Arguments

| Argument | Description                                                          |
|----------|----------------------------------------------------------------------|
| browser  | Initializes configuration for code running in the browser.           |
| node     | Initializes configuration for code running on node or an NPM ibrary. |

## What This Provides

- TypeScript
- ESLint
- StyleLint
- Git Hooks
- Vite
- Vitest
- SWC
- NPM Scripts
  - With Cypress Scripts, Not Cypress
- Auto-Typing for CSS/SCSS Modules

## Test Support

### jest-fp-ts

The `@relmify/jest-fp-ts` package will be automatically setup in tests when that package is present in the application.

## Git Hooks

### Pre-Commit

Before each commit, eslint & stylelint will be run on the appropriate files.

## Commands

| Command       | Arguments                          | Description                                                                                                                            |
|---------------|------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------|
| c-build-app   | None                               | Build a full application with vite                                                                                                     |
| c-build-lib   | -c = cjs only, -e = esm only       | Build a library with SWC & TypeScript                                                                                                  |
| c-cypress     | None                               | Run the cypress suite headless (requires Cypress). Pass --parallel-total=# and --parallel-index=# to configure for parallel execution. |
| c-cypress-dev | None                               | Run the cypress dev tool (requires Cypress)                                                                                            |
| c-eslint      | An optional path to a file to lint | Run eslint on either a single file or the whole project.                                                                               |
| c-init        | None                               | Initialize the whole project. Automatically runs after installing.                                                                     |
| c-log         | None                               | See the logs created by these commands to debug issues.                                                                                |
| c-start       | Any valid Vite dev server args.    | Start the vite dev server                                                                                                              |
| c-stylelint   | An optional path to a file to lint | Run stylelint on either a single file or the whole project.                                                                            |
| c-test        | None                               | Run the vitest suite                                                                                                                   |
| c-type-check  | None                               | Perform TypeScript type checking on the codebase.                                                                                      |
| c-validate    | None                               | Run all validation scripts for the project.                                                                                            |
| c-type-styles | None                               | Generate types for CSS & SCSS Modules                                                                                                  |

## Scripts In Other Projects

Other projects trying to emulate the scripts in this one can do so by importing the contents of `@craigmiller160/js-config/script-support`. Everything in that module is intended to support scripts in other packages.

## Other Features

### Cypress Component Testing & the Vite Environment

To workaround issues with cypress component testing and the vite environment, the `c-cypress-dev` and `c-cypress` scripts will check for the presence of a file at `environment/.env.cypress`. If present, the contents will be explicitly loaded as environment variables prior to running the cypress commands.